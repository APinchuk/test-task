New candidate tasks
===============

Test task for Junior/Middle Web (PHP) developer.

## Setup
You need PHP 5.3.8+ (5.5 preferable) and [composer](https://getcomposer.org/) to install dependencies.

Setup:
```shell
$ git clone https://APinchuk@bitbucket.org/APinchuk/test-task.git
$ cd test-task
$ composer install
```

## Tasks

Feel free to explore `src` directory for available tasks. Each tasks contains it's own `README.md`, with tasks explanation.


## Submission
You should submit your solution in the way:
- Send your results via email

